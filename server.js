const Hapi = require('@hapi/hapi');
const routes=require('./routes/router')
const cors=true;
const server = Hapi.server({
    port: 4000,
    host: 'localhost',
    routes:{
        cors:{
            origin:['http://localhost:3001']
        }
    }
});
server.start()

.then(()=>{
    console.log(`Server running on port 4000`);
})
.catch(err=>{
    console.log(err);
})
server.route(routes)


module.exports=server;