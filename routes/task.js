var taskController=require('../controller/task.controller')

module.exports = [
    { method: 'GET', path: '/task/{userId}', config: taskController.getUserTask },
    ];