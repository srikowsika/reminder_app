var userController=require('../controller/user.controller')

module.exports = [
    { method: 'GET', path: '/users', config: userController.getUsers },
    { method: 'GET', path: '/users/{managerId}', config: userController.getUsersByManagerId}
    ];