var groupController=require('../controller/group.controller')

module.exports = [
    { method: 'GET', path: '/groups/{groupId}', config: groupController.getGroupMembers },
    { method: 'GET', path: '/groups/user/{userId}', config: groupController.getUserGroups }
    ];