const connection = require('./db')
const config=require('../config/config.json')

getGroupMembers={
    handler: async (request,h)=>{
        var response;
        await new Promise((resolve,reject)=>{
            connection.query(config.query.groupMembers,request.params.groupId, (error,results) => {
                if (error) reject(error);
                resolve(results)
            }) 
        })
        .then(groupMembers=>{
            response={
                statusCode:'200',
                statusMessage:'OK',
                message:'Group members',
                data:groupMembers,
            }
        })
        // .catch(error=>{
        //     response={
        //         statusCode:'200',
        //         statusMessage:'OK',
        //         message:'Group members',
        //         data:error,
        //     }
        // })   
        return response
    }  
}

getUserGroups={
    handler: async (request,reply)=>{
        groups= await new Promise((resolve,reject)=>{
            connection.query(config.query.userGroups,request.params.userId, (error,results) => {
                if (error) reject(error);
                resolve(results)
            }) 
        })
        var response={
            statusCode:'200',
            statusMessage:'OK',
            message:'Groups of the user',
            data:groups,
        }
    return response
    }  
}


module.exports={
    getGroupMembers,
    getUserGroups
}


