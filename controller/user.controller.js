const connection = require('./db')
const config=require('../config/config.json')

getUsers={
    handler: async (request,reply)=>{
        userDetails= await new Promise((resolve,reject)=>{
            connection.query(`select * from user`, (error,results) => {
                if (error) reject(error);
                resolve(results)
            }) 
        })
        return userDetails
    }  
}
getUsersByManagerId={
    handler: async (request,reply)=>{
        userDetails= await new Promise((resolve,reject)=>{
            connection.query(config.query.userUnderAManager,request.params.managerId, (error,results) => {
                if (error) reject(error);
                resolve(results)
            }) 
        })
        return userDetails
    }  
}


module.exports={
    getUsers,
    getUsersByManagerId
}


