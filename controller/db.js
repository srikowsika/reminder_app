var mysql      = require('mysql');
var db=require('../config/config.json')

const connection = mysql.createConnection({
  host     : db.mysqlConnect.host,
  user     : db.mysqlConnect.user,
  password : db.mysqlConnect.password,
  database : db.mysqlConnect.database
});
connection.connect();
   

module.exports=connection;