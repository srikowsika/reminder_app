var connection=require('./db')
const config=require('../config/config.json')

getUserTask={
    handler: async (request,reply)=>{
        taskDetails= await new Promise((resolve,reject)=>{
            connection.query(config.query.userTask,request.params.userId, (error,results) => {
                if (error) reject(error);
                resolve(results)
            }) 
        })
    return taskDetails
    }  
}
module.exports={
    getUserTask
}